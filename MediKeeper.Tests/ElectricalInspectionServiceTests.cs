﻿using System;
using MediKeeper.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MediKeeper.Tests
{
    [TestClass]
    public class ElectricalInspectionServiceTests
    {
        private IElectricalInspectionService inspectionService;

        [TestInitialize]
        public void Setup()
        {
            inspectionService = new ElectricalInspectionService();
        }

        [TestMethod]
        public void Inspection_Should_Have_Inspector_Name()
        {
            var inspection = inspectionService.CreateInspection(12);
            Assert.IsNotNull(inspection.InspectorName);
        }

        [TestMethod]
        public void Inspection_Outlet_Inspection_Incorrect_Height_Should_Not_Pass()
        {
            var inspection = inspectionService.CreateInspection(8);
            Assert.IsFalse(inspection.PassedInspection);
        }

        [TestMethod]
        public void Inspection_Outlet_Inspection_Correct_Height_Should_Pass()
        {
            var inspection = inspectionService.CreateInspection(12);
            Assert.IsTrue(inspection.PassedInspection);
        }

        [TestCleanup]
        public void Cleanup()
        {
            inspectionService = null;
        }
    }
}
