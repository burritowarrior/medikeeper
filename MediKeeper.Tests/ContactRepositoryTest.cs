﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediKeeper.Interfaces;
using MediKeeper.Models;

namespace MediKeeper.Tests
{
    public class ContactRepositoryTest 
    {
    }

    public class ContactRepository : IContactRepository
    {
        private List<Contact> contactList;

        public ContactRepository()
        {
            contactList = new List<Contact>
            {
                new Contact { ContactId = 2785, FirstName = "Joe", LastName = "Smith", Address = "155 Main St", City = "My City", State = "CA", ZipCode = "11111", Phone = "1112223333", DateEntered = DateTime.UtcNow },
                new Contact { ContactId = 2786, FirstName = "Jane", LastName = "Smith", Address = "155 Main St", City = "My City", State = "CA", ZipCode = "11111", Phone = "1112223333", DateEntered = DateTime.UtcNow },
            };    
        }

        public IEnumerable<Contact> GetAll(out string message)
        {
            throw new NotImplementedException();
        }

        public Contact GetById(int id, string procName, out string message)
        {
            message = contactList.Any(c => c.ContactId == id) ? "Operation Successful" : "Operation Failure";

            return contactList.First(f => f.ContactId == id);
        }

        public bool AddOrUpdate(Contact contact, string procName, bool isUpdate, out string message)
        {
            throw new NotImplementedException();
        }

        public bool DeleteContact(int id, string procName, out string message)
        {
            throw new NotImplementedException();
        }
    }
}
