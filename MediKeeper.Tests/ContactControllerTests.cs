﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using MediKeeper.Controllers;
using MediKeeper.Helpers;
using MediKeeper.Interfaces;
using MediKeeper.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

// https://docs.microsoft.com/en-us/aspnet/web-api/overview/testing-and-debugging/unit-testing-controllers-in-web-api
namespace MediKeeper.Tests
{
    [TestClass]
    public class ContactControllerTests
    {
        [TestInitialize]
        public void Setup()
        {
            // inspectionService = new ElectricalInspectionService();
        }

        [TestMethod]
        public void Product_Exists_And_Returns_Correct_Response()
        {
            // Arrange - The London style definitely has some benefits as opposed to classic
            //var message = string.Empty;
            //var mockRepository = new Mock<IContactRepository>();
            //mockRepository.Setup<Contact>(x => x.GetById(2785, string.Empty, out message))
            //    .Returns(new Contact { ContactId = 2785 });
            IContactRepository contactRepository = new ContactRepository();
            var controller = new ContactController(contactRepository);

            // Act
            IHttpActionResult actionResult = controller.GetContactById(2785);

            var contentResult = actionResult as OkNegotiatedContentResult<JsonPayload<Contact>>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(2785, contentResult.Content.Payload.ContactId);
        }
    }
}
