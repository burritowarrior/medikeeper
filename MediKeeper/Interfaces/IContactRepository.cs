﻿using System.Collections.Generic;
using MediKeeper.Models;

namespace MediKeeper.Interfaces
{
    public interface IContactRepository
    {
        IEnumerable<Contact> GetAll(out string message);
        Contact GetById(int id, string procName, out string message);
        bool AddOrUpdate(Contact contact, string procName, bool isUpdate, out string message);
        bool DeleteContact(int id, string procName, out string message);
    }
}