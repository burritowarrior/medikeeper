﻿using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web.Http;
using Dapper;

namespace MediKeeper.Controllers
{
    public class HomeController : ApiController
    {
        private readonly string DATA_CONNECTION = ConfigurationManager.ConnectionStrings["AzureConnection"].ConnectionString;

        [Route("RetrieveMaxPriceByItem/{ItemName}")]
        [HttpGet]
        public IHttpActionResult GetValue(string itemName)
        {
            var number = 0.0M;
            var param = new DynamicParameters();
            param.Add("@ItemName", itemName, DbType.AnsiString, ParameterDirection.Input);

            // new { @ItemName = itemName },
            using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
            {
                number = db.Query<decimal>(
                    "dbo.usp_LookupByItemName",
                    param,
                    commandType: CommandType.StoredProcedure
                ).FirstOrDefault();
            }

            if (number == 0)
                return Content(HttpStatusCode.NotFound, $"{itemName} does not exist.");

            return Ok(new { success = true, payload = number });
        }

        [Route("GetAllItems")]
        [HttpGet]
        public IHttpActionResult GetValues()
        {
            List<Item> items;
            using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
            {

                items = db.Query<Item>("SELECT Id, [Item Name] AS ItemName, Cost FROM Items").ToList();
            }

            return Ok(new { success = true, payload = items });
        }

        [Route("RetrieveMaxPrices")]
        [HttpGet]
        public IHttpActionResult GetMaxPrices()
        {
            List<Item> items;
            using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
            {

                items = db.Query<Item>("SELECT [Item Name] AS ItemName, MaxCost AS Cost FROM vw_MaxPricesGrouped").ToList();
            }

            return Ok(new { success = true, payload = items });
        }

        [Route("AddNewItem")]
        [HttpPost]
        public IHttpActionResult AddItem(Item item)
        {
            var param = new DynamicParameters();
            param.Add("@ItemName", item.ItemName, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@Cost", item.Cost, DbType.Decimal, ParameterDirection.Input);

            using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
            {
                db.Execute("dbo.usp_AddNewItem", param, null, null, CommandType.StoredProcedure);
            }

            return Ok(new { success = true, payload = "Item Added" });
        }

        [Route("DeleteItem/{Id}")]
        [HttpDelete]
        public IHttpActionResult RemoveItem(string id)
        {
            var param = new DynamicParameters();
            param.Add("@Id", id, DbType.AnsiString, ParameterDirection.Input);

            using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
            {
                db.Execute("dbo.usp_DeleteItem", param, null, null, CommandType.StoredProcedure);
            }

            return Ok(new { success = true, payload = $"Item {id} Deleted" });
        }

        [Route("UpdateItem")]
        [HttpPut]
        public IHttpActionResult ModifyItem(Item item)
        {
            var param = new DynamicParameters();
            param.Add("@Id", item.Id, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@ItemName", item.ItemName, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@Cost", item.Cost, DbType.Decimal, ParameterDirection.Input);

            using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
            {
                db.Execute("dbo.usp_UpdateItem", param, null, null, CommandType.StoredProcedure);
            }

            return Ok(new { success = true, payload = $"Item {item.Id} Updated" });
        }
    }

    public class Item
    {
        public string Id { get; set; }
        public string ItemName { get; set; }
        public decimal Cost { get; set; }
    }
}
