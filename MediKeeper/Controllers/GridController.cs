﻿using System.Net;
using System.Web.Http;
using MediKeeper.Models;
using MediKeeper.Repositories;

namespace MediKeeper.Controllers
{
    public class GridController : ApiController
    {
        private readonly object EMPTY_OBJECT = new object();

        [Route("GetAllUsers")]
        [HttpGet]
        public IHttpActionResult GetUsers()
        {
            var items = GridRepository.GetAllUsers(out var message);
            var httpStatusCode = items.Count > 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;

            return Ok(new { success = items.Count > 0, statusCode = httpStatusCode, payload = items, statusMessage = message });
        }

        [Route("AddNewUser")]
        [HttpPost]
        public IHttpActionResult AddUser(User item)
        {
            var status = GridRepository.AddOrModifyItem(item, "dbo.usp_AddNewUser", out var message);
            var httpStatusCode = status != 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;

            return Ok(new { success = status != 0, statusCode = httpStatusCode, payload = EMPTY_OBJECT, statusMessage = message });
        }

        [Route("UpdateUser")]
        [HttpPut]
        public IHttpActionResult UpdateUser(User item)
        {
            var status = GridRepository.AddOrModifyItem(item, "dbo.usp_UpdateUser", out var message, true);
            var httpStatusCode = status != 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;

            return Ok(new { success = status != 0, statusCode = httpStatusCode, payload = EMPTY_OBJECT, statusMessage = message });
        }

        [Route("DeleteUser")]
        [HttpDelete]
        public IHttpActionResult DeleteUser(User item)
        {
            var status = GridRepository.AddOrModifyItem(item, "dbo.usp_DeleteUser", out var message, false, true);
            var httpStatusCode = status != 0 ? HttpStatusCode.OK : HttpStatusCode.BadRequest;

            return Ok(new { success = status != 0, statusCode = httpStatusCode, payload = EMPTY_OBJECT, statusMessage = message });
        }
    }
}
