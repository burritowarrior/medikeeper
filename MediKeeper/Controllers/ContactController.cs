﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MediKeeper.Helpers;
using MediKeeper.Interfaces;
using MediKeeper.Models;
using Newtonsoft.Json;
using DbActions = MediKeeper.Helpers.DatabaseProcedures;

namespace MediKeeper.Controllers
{
    public class ContactController : ApiController
    {
        private readonly IContactRepository contactRepository;

        public ContactController(IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        [Route("GetAllContacts")]
        [HttpGet]
        public IHttpActionResult GetContacts()
        {
            var contacts = contactRepository.GetAll(out var statusMessage);
            if (contacts == null)
                return BadRequest(statusMessage);

            var results = new JsonCollection<Contact>(statusMessage, HttpStatusCode.OK)
            {
                DataCollection = contacts
            };

            return Ok(results);
        }

        [Route("GetContact/{id}")]
        [HttpGet]
        public IHttpActionResult GetContactById(int id)
        {
            var contact = contactRepository.GetById(id, null, out var statusMessage);
            if (contact == null)
                return BadRequest(statusMessage);

            var results = new JsonPayload<Contact>(statusMessage, HttpStatusCode.OK)
            {
                Payload = contact
            };

            return Ok(results);
        }

        [Route("AddNewContact")]
        [HttpPost]
        public IHttpActionResult AddContact(object value)
        {
            var contact = JsonConvert.DeserializeObject<Contact>(value.ToString());
            var dbUpdated = contactRepository.AddOrUpdate(contact, DbActions.Actions["AddContact"], false, out string message);

            if (!dbUpdated)
                return BadRequest(message);

            return Ok(new JsonResponseHelper());
        }

        [Route("UpdateExistingContact")]
        [HttpPut]
        public IHttpActionResult UpdateContact(object value)
        {
            var contact = JsonConvert.DeserializeObject<Contact>(value.ToString());
            var dbUpdated = contactRepository.AddOrUpdate(contact, DbActions.Actions["UpdateContact"], true, out string message);

            if (!dbUpdated)
                return BadRequest(message);

            return Ok(new JsonResponseHelper());
        }

        [Route("DeleteContact/{id}")]
        [HttpDelete]
        public IHttpActionResult RemoveContact(string id)
        {
            var contactId = Convert.ToInt32(id);
            var dbUpdated = contactRepository.DeleteContact(contactId, DbActions.Actions["DeleteContact"], out var message);
            if (!dbUpdated)
                return BadRequest(message);

            return Ok(new JsonResponseHelper());
        }
    }
}
