IF OBJECT_ID('Contact') IS NOT NULL 
	DROP TABLE Contact;

CREATE TABLE Contact
(
	[ContactId]		INT				NOT NULL	IDENTITY(2783, 1),
	[FirstName]		VARCHAR(64)		NOT NULL,
	[LastName]		VARCHAR(64)		NOT NULL,
	[Address]		VARCHAR(255)	NOT NULL,
	[City]			VARCHAR(64)		NOT NULL,
	[State]			VARCHAR(32)		NOT NULL,
	[ZipCode]		VARCHAR(12)		NOT NULL,
	[Phone]			VARCHAR(12)		NOT NULL,
	[DateEntered]	DATETIME		NOT NULL
)
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_SelectAllContacts]
AS
SET NOCOUNT ON

SELECT ContactId, FirstName, LastName, [Address], City, [State], ZipCode, Phone, CONVERT(VARCHAR(10), DateEntered, 110) AS DateEntered FROM Contact

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_SelectContactById]
(
	@ContactId	INT
)
AS 
SET NOCOUNT ON

SELECT ContactId, FirstName, LastName, [Address], City, [State], ZipCode, Phone, DateEntered FROM Contact WHERE ContactId = @ContactId

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_InsertContact]
(
	@FirstName	VARCHAR(64),
	@LastName	VARCHAR(64),
	@Address	VARCHAR(255),
	@City		VARCHAR(64),
	@State		VARCHAR(32),
	@ZipCode	VARCHAR(12),
	@Phone		VARCHAR(12)
)
AS
SET NOCOUNT ON

DECLARE @RowsModified	INT = 0

INSERT INTO Contact (FirstName, LastName, [Address], City, [State], ZipCode, Phone, DateEntered)
VALUES (@FirstName, @LastName, @Address, @City, @State, @ZipCode, @Phone, GETUTCDATE())

SELECT @RowsModified = @@ROWCOUNT

RETURN @RowsModified

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_UpdateContact]
(
	@ContactId	INT,
	@FirstName	VARCHAR(64),
	@LastName	VARCHAR(64),
	@Address	VARCHAR(255),
	@City		VARCHAR(64),
	@State		VARCHAR(32),
	@ZipCode	VARCHAR(12),
	@Phone		VARCHAR(12)
)
AS
SET NOCOUNT ON

DECLARE @RowsModified	INT = 0

UPDATE Contact SET
	FirstName = @FirstName,
	LastName = @LastName,
	Address = @Address,
	City = @City,
	State = @State,
	ZipCode = @ZipCode,
	Phone = @Phone,
	DateEntered = GETUTCDATE()
WHERE ContactId = @ContactId

SELECT @RowsModified = @@ROWCOUNT

RETURN @RowsModified

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_DeleteContact]
(
	@ContactId	INT
)
AS
SET NOCOUNT ON

DECLARE @RowsModified	INT = 0

DELETE FROM Contact WHERE ContactId = @ContactId

SELECT @RowsModified = @@ROWCOUNT

RETURN @RowsModified

SET NOCOUNT OFF
GO

EXEC usp_InsertContact 'Joseph', 'Tannenbaum', '234 Amethyst Dr', 'Vacaville', 'CA', '94589', '9164827799';
EXEC usp_InsertContact 'Jennifer', 'Parker', '9821 Benton Ave #519', 'Plano', 'TX', '73194', '2159384711';
EXEC usp_InsertContact 'Maria', 'De La Cruz', '6472 Plymouth Dr', 'Minneapolis', 'MN', '55312', '6197189044';
EXEC usp_InsertContact 'Richard', 'Yee', '5781 Lynwood Dr', 'Boston', 'MA', '01764', '6173984412';
EXEC usp_InsertContact 'Sarah', 'Wilkins', '384 San Clemente Rd', 'Austin', 'TX', '77384', '5129410909';

SELECT * FROM Contact;

EXEC usp_SelectContactById 2784;
EXEC usp_DeleteContact 2793;

/*
DELETE FROM Contact WHERE ContactId BETWEEN 2794 AND 2796;
SELECT @procName = OBJECT_NAME(@@PROCID)

-- For the next data type
SELECT @FirstName + ', ' + @LastName

SELECT @results = @results + CASE WHEN ParameterDataType = VARCHAR THEN CAST(@FirstName AS VARCHAR) ELSE CAST (@SomeValue AS VARCHAR) END 
FROM ??? WHERE SO.Name = @procName

SELECT SCHEMA_NAME(SCHEMA_ID) AS [Schema],
SO.name AS [ObjectName],
SO.Type_Desc AS [ObjectType (UDF/SP)],
P.parameter_id AS [ParameterID],
P.name AS [ParameterName],
TYPE_NAME(P.user_type_id) AS [ParameterDataType],
P.max_length AS [ParameterMaxBytes],
P.is_output AS [IsOutPutParameter]
FROM sys.objects AS SO
INNER JOIN sys.parameters AS P
ON SO.OBJECT_ID = P.OBJECT_ID
WHERE SO.OBJECT_ID IN ( SELECT OBJECT_ID
FROM sys.objects
WHERE TYPE IN ('P','FN')) AND SO.Name = 'usp_InsertContact'
ORDER BY [Schema], SO.name, P.parameter_id
GO
*/