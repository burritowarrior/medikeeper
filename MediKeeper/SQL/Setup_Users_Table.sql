﻿USE [Development]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 8/12/2020 2:22:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(2341,1) NOT NULL,
	[Name] [varchar](65) NOT NULL,
	[Age] [int] NULL,
	[Country] [int] NULL,
	[EmailAddress] [varchar](255) NULL,
	[Married] [bit] NULL,
	[DateEntered] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Users] ON 
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2341, N'Roberta Flack', 33, 3, N'robertaflack@gmail.com', 1, CAST(N'2020-08-12T21:12:46.180' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2342, N'Angelita Flanders', 28, 1, N'qqoqu7828@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2343, N'Rudolph Crabtree', 45, 3, N'MargheritaHaight@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2344, N'Major Brent', 36, 1, N'pxohxywl_jckisyzuql@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2345, N'Andrea Mora', 40, 0, N'TessaLundy@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2346, N'Quincy Wade', 45, 0, N'yapbjb78@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2347, N'Carrol Mcclintock', 21, 1, N'Acuna491@nowhere.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2348, N'Yajaira Singleton', 38, 0, N'Collin.Rocha9@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2349, N'Kandis Quinonez', 45, 2, N'LeonieThomas639@nowhere.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2350, N'Gerard Flannery', 33, 2, N'Raymon_Coward@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2351, N'Grover Wadsworth', 38, 2, N'JerryM_Coble@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2352, N'Corrin Alger', 36, 2, N'ColettaCollins@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2353, N'Refugio Kirkwood', 40, 1, N'CharleyChau@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2354, N'Penney Craddock', 35, 3, N'Hooks@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2355, N'Chong Morales', 54, 2, N'Ernesto_Pina154@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2356, N'Charley Fleming', 26, 3, N'Sharkey49@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2357, N'Devin Craft', 46, 3, N'Mackey@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2358, N'Howard Quintana', 24, 1, N'Zielinski2@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2359, N'Obdulia Mccloskey', 30, 0, N'Conrad@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2360, N'Amos Sipes', 44, 0, N'Eugena_Prichard@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2361, N'Elanor Moran', 37, 0, N'DanielKelsey@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2362, N'Wendolyn Waggoner', 37, 2, N'LolaY.Butler121@nowhere.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2363, N'Andrea Flemming', 45, 0, N'cumqhzrc.guyqayyay@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2364, N'Jaqueline Craig', 54, 1, N'Roberts@nowhere.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2365, N'Earle Quintanilla', 51, 1, N'Lujan@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2366, N'Ike Kiser', 32, 2, N'godj25@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2367, N'Hipolito Moreau', 27, 2, N'Clement@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2368, N'Wilford Mccloud', 38, 2, N'PaulinaO_Angel858@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2369, N'Anderson Kitchen', 54, 2, N'Knudsen@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2370, N'Marquis Mcclung', 39, 0, N'AlexisRoby8@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2371, N'Hilton Kitchens', 43, 0, N'Lima73@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2372, N'Raul Sisco', 40, 1, N'RoseliaAlonzo786@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2373, N'Edmond Wagner', 35, 3, N'Dustin.Mullin@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2374, N'Dayna Sisk', 28, 0, N'ArielAcker@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2375, N'Ricarda Wagoner', 34, 2, N'Anaya@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2376, N'Casie Sisson', 47, 1, N'HarryCallahan@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2377, N'Alla Quintero', 22, 1, N'Sparks27@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2378, N'Kip Brewer', 49, 3, N'Cushman1@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2379, N'Adam Wahl', 46, 1, N'ucoc2205@nowhere.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2380, N'Jeffery Ali', 32, 3, N'Lamb@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2381, N'Natividad Morehead', 45, 0, N'Abreu@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2382, N'Agustin Hargis', 26, 2, N'Felicitas_Cosby651@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2383, N'Otis Fletcher', 53, 2, N'BryannaAbraham@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2384, N'Tyler Klein', 29, 1, N'ThomasinaLeone@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2385, N'Alonzo Sizemore', 42, 1, N'Mccain@nowhere.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2386, N'Abram Waite', 34, 1, N'mrak7210@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2387, N'Fae Hargrove', 50, 1, N'Wallis@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2388, N'Buster Brewster', 52, 2, N'Abby.Ambrose@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2389, N'Hollis Alicea', 55, 3, N'Rod_Link7@example.com', 1, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2390, N'Wilmer Skaggs', 28, 3, N'QuintinSorensen@example.com', 0, CAST(N'2020-08-12T20:16:36.937' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2391, N'Bob Perry', 35, 1, N'bobperry@gmail.com', 1, CAST(N'2020-08-12T20:40:23.283' AS DateTime))
GO
INSERT [dbo].[Users] ([UserId], [Name], [Age], [Country], [EmailAddress], [Married], [DateEntered]) VALUES (2392, N'Brenda Wilson', 59, 1, N'brendawilson@gmail.com', 0, CAST(N'2020-08-12T20:45:55.430' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
