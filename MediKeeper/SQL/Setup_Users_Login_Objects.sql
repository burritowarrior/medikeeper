﻿USE [master]
GO
CREATE LOGIN [sqluser] WITH PASSWORD=N'sqluser', DEFAULT_DATABASE=[Development], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

USE [Development]
GO
CREATE USER [sqluser] FOR LOGIN [sqluser] WITH DEFAULT_SCHEMA=[dbo]
GO

EXEC sp_addrolemember N'db_owner', N'sqluser';
GO

CREATE PROCEDURE [dbo].[usp_GetAllUsers]
AS
SET NOCOUNT ON

SELECT [Name], Age, Country, EmailAddress, Married FROM Users

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_AddNewUser]
(
	@Name			VARCHAR(65),
	@Age			INT,
	@Country		INT,
	@EmailAddress	VARCHAR(255),
	@Married		BIT
)
AS
SET NOCOUNT ON

INSERT INTO Users (Name, Age, Country, EmailAddress, Married, DateEntered) VALUES (@Name, @Age, @Country, @EmailAddress, @Married, GETUTCDATE())

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_UpdateUser]
(
	@UserId			INT,
	@Name			VARCHAR(65),
	@Age			INT,
	@Country		INT,
	@EmailAddress	VARCHAR(255),
	@Married		BIT
)
AS
SET NOCOUNT ON

UPDATE Users SET
	[Name] = @Name,
	Age = @Age,
	Country = @Country,
	EmailAddress = @EmailAddress,
	Married = @Married, 
	DateEntered = GETUTCDATE()
WHERE UserId = @UserId

SET NOCOUNT OFF
GO

CREATE OR ALTER PROCEDURE [dbo].[usp_DeleteUser]
(
	@UserId			INT
)
AS
SET NOCOUNT ON

DELETE FROM Users WHERE UserId = @UserId 

SET NOCOUNT OFF
GO
