﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediKeeper.Models
{
    public class Contact
    {
        public int? ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public DateTime? DateEntered { get; set; }
    }
}