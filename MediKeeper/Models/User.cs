﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediKeeper.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public int Country { get; set; }
        public string EmailAddress { get; set; }
        public bool Married { get; set; }
    }
}