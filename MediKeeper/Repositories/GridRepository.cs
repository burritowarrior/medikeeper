﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Policy;
using System.Web.Caching;
using Dapper;
using MediKeeper.Models;

namespace MediKeeper.Repositories
{
    public class GridRepository
    {
        private static readonly string DATA_CONNECTION = ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString;

        public static List<User> GetAllUsers(out string message)
        { 
            List<User> items;

            try
            {

                using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
                {

                    items = db.Query<User>("SELECT UserId, [Name], Age, Country, EmailAddress, CAST(Married AS BIT) AS Married FROM Users")
                        .ToList();
                    message = "Data Retrieval successful";
                }
            }
            catch (Exception ex)
            {
                items = null;
                message = ex.Message;
            }

            return items;
        }

        public static int AddOrModifyItem(User item, string procName, out string message, bool modifyByUserId = false, bool deleteByUserId = false)
        {
            var results = 0;
            try
            {
                var param = new DynamicParameters();

                if (modifyByUserId || deleteByUserId)
                {
                    param.Add("@UserId", item.UserId, DbType.Int32, ParameterDirection.Input);
                }

                if (!deleteByUserId)
                {
                    param.Add("@Name", item.Name, DbType.AnsiString, ParameterDirection.Input);
                    param.Add("@Age", item.Age, DbType.Int32, ParameterDirection.Input);
                    param.Add("@Country", item.Country, DbType.Int32, ParameterDirection.Input);
                    param.Add("@EmailAddress", item.EmailAddress, DbType.AnsiString, ParameterDirection.Input);
                    param.Add("@Married", item.Married, DbType.Boolean, ParameterDirection.Input);
                }
                
                using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
                {
                    results = db.Execute(procName, param, null, null, CommandType.StoredProcedure);
                    message = $"{procName} successful";
                }
            }
            catch (Exception ex)
            {
                // Console.WriteLine(ex);
                message = ex.Message;
                throw;
            }

            return results;
        }
    }
}