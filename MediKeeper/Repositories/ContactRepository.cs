﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using MediKeeper.Helpers;
using MediKeeper.Interfaces;
using MediKeeper.Models;

namespace MediKeeper.Repositories
{
    public class ContactRepository : IContactRepository
    {
        private static readonly string DATA_CONNECTION = ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString;

        public IEnumerable<Contact> GetAll(out string message)
        {
            Contact[] items;
            var sqlQuery = "SELECT ContactId, FirstName, LastName, [Address], City, [State], ZipCode, Phone, DateEntered FROM Contact";

            try
            {
                using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
                {
                    items = db.Query<Contact>(sqlQuery).ToArray();
                    message = "Data Retrieval successful";
                }
            }
            catch (SqlException se)
            {
                items = null;
                message = se.Message;
            }
            catch (Exception ex)
            {
                items = null;
                message = ex.Message;
            }

            return items;
        }

        public Contact GetById(int id, string procName, out string message)
        {
            Contact contact;
            var sqlQuery = $"SELECT ContactId, FirstName, LastName, [Address], City, [State], ZipCode, Phone, DateEntered FROM Contact WHERE ContactId = {id}";

            try
            {
                using (IDbConnection db = new SqlConnection(DATA_CONNECTION))
                {
                    contact = db.Query<Contact>(sqlQuery).SingleOrDefault();
                    message = "Data Retrieval successful";
                }
            }
            catch (SqlException se)
            {
                contact = null;
                message = se.Message;
            }
            catch (Exception ex)
            {
                contact = null;
                message = ex.Message;
            }

            return contact;
        }

        public bool AddOrUpdate(Contact contact, string procName, bool isUpdate, out string message)
        {
            var param = new DynamicParameters();

            if (isUpdate)
                param.Add("@ContactId", contact.ContactId, DbType.Int32, ParameterDirection.Input);

            param.Add("@FirstName", contact.FirstName, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@LastName", contact.LastName, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@Address", contact.Address, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@City", contact.City, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@State", contact.State, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@ZipCode", contact.ZipCode, DbType.AnsiString, ParameterDirection.Input);
            param.Add("@Phone", contact.Phone, DbType.AnsiString, ParameterDirection.Input);

            param.Add("@RowsModified", null, DbType.Int32, ParameterDirection.ReturnValue);

            var executionHelper = new ExecuteActionHelper();
            var results = executionHelper.ExecuteAction(param, procName);
            message = results.Message;

            return results.RecordsAffected > 0;
        }

        public bool DeleteContact(int id, string procName, out string message)
        {
            var param = new DynamicParameters();

            param.Add("@ContactId", id, DbType.Int32, ParameterDirection.Input);
            param.Add("@RowsModified", null, DbType.Int32, ParameterDirection.ReturnValue);

            var executionHelper = new ExecuteActionHelper();
            var results = executionHelper.ExecuteAction(param, procName);
            message = results.Message;

            return results.RecordsAffected > 0;
        }
    }
}