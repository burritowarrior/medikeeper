﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using MediKeeper.Interfaces;
using MediKeeper.Models;
using MediKeeper.Repositories;
using Unity;

namespace MediKeeper
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();
            container.RegisterType<IContactRepository, ContactRepository>();
            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
