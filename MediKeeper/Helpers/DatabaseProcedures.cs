﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediKeeper.Helpers
{
    public static class DatabaseProcedures
    {
        public static Dictionary<string, string> Actions { get; } = new Dictionary<string, string>
        {
            { "AddContact", "usp_InsertContact" },
            { "UpdateContact", "usp_UpdateContact" },
            { "DeleteContact", "usp_DeleteContact" },
            { "GetContact", "usp_SelectContactById" }
        };
    }
}