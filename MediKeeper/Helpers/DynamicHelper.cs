﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MediKeeper.Helpers
{
    public static class DynamicHelper
    {
        public static T ToStaticType<T>(this object item)
        {
            var instanceType = Activator.CreateInstance<T>();

            var properties = item as IDictionary<string, object>;

            if (properties == null) return instanceType;

            foreach (var prop in properties)
            {
                var propertyInfo = instanceType.GetType().GetProperty(prop.Key);
                if (propertyInfo != null)
                {
                    propertyInfo.SetValue(instanceType, prop.Value, null);
                }
            }

            return instanceType;
        }
    }
}