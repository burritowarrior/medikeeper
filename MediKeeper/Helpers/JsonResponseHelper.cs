﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace MediKeeper.Helpers
{
    public class JsonResponseHelper
    {
        public string Message { get; set; }
        public HttpStatusCode StatusCode { get; set; }

        public JsonResponseHelper()
        {
            Message = "Operation Successful";
            StatusCode = HttpStatusCode.OK;
        }

        public JsonResponseHelper(string message, HttpStatusCode statusCode)
        {
            Message = message;
            StatusCode = statusCode;
        }
    }

    public class JsonPayload<T> : JsonResponseHelper
    {
        public T Payload { get; set; }

        public JsonPayload(string message, HttpStatusCode statusCode)
        {
            base.Message = message;
            base.StatusCode = statusCode;
        }
    }

    public class JsonCollection<T> : JsonResponseHelper
    {
        public IEnumerable<T> DataCollection { get; set; }

        public JsonCollection(string message, HttpStatusCode statusCode)
        {
            base.Message = message;
            base.StatusCode = statusCode;
        }
    }
}