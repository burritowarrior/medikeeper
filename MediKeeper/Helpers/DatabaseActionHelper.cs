﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace MediKeeper.Helpers
{
    public class DatabaseActionResult
    {
        public int RecordsAffected { get; set; }
        public string Message { get; set; }
    }

    public class ExecuteActionHelper
    {
        public DatabaseActionResult ExecuteAction(DynamicParameters parameters, string procName)
        {
            string databaseConnectionString = ConfigurationManager.ConnectionStrings["LocalConnection"].ConnectionString;
            var results = 0;
            string message;

            try
            {
                using (IDbConnection db = new SqlConnection(databaseConnectionString))
                {
                    results = db.Execute(procName, parameters, null, null, CommandType.StoredProcedure);
                    message = $"{procName} successful";

                    results = parameters.Get<int>("@RowsModified");
                }
            }
            catch (SqlException se)
            {
                message = se.Message;
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new DatabaseActionResult
            {
                Message = message,
                RecordsAffected = results
            };
        }
    }
}