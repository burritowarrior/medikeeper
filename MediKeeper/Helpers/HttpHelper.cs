﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace MediKeeper.Helpers
{
    public static class HttpHelper
    {
        public static HttpStatusCode ResolveResults(this bool input)
        {
            return input ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }

        public static HttpStatusCode ResolveCollection<T>(this IEnumerable<T> input)
        {
            if (input == null) return HttpStatusCode.BadRequest;

            return input.Any() ? HttpStatusCode.OK : HttpStatusCode.BadRequest;
        }
    }
}