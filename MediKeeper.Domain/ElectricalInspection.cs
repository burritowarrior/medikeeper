﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediKeeper.Domain
{
    public class ElectricalInspection
    {
        public int ReceptacleInstallationHeight { get; set; }
        public bool PassedInspection { get; set; }
        public string InspectorName { get; set; }
    }
}
