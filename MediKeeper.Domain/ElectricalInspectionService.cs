﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediKeeper.Domain
{
    public class ElectricalInspectionService : IElectricalInspectionService
    {
        private const int INSTALLATION_HEIGHT_IN_INCHES = 12;
        public ElectricalInspection CreateInspection(int outletInstallationHeight)
        {
            var electricalInspection = new ElectricalInspection
            {
                ReceptacleInstallationHeight = outletInstallationHeight,
                PassedInspection = outletInstallationHeight == INSTALLATION_HEIGHT_IN_INCHES,
                InspectorName = "John Doe"
            };

            return electricalInspection;
        }
    }
}
