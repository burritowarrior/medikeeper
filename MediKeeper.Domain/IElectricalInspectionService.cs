﻿namespace MediKeeper.Domain
{
    public interface IElectricalInspectionService
    {
        ElectricalInspection CreateInspection(int outletInstallationHeight);
    }
}